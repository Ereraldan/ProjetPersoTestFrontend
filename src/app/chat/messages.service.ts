import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Message } from './message';

@Injectable()
export class MessagesService {

  constructor(private http: HttpClient) {
  }

  send(content: string, idRoom: string) {
    const message = new Message();
    message.room.id = idRoom;
    message.content = content;
    this.http.post(environment.url + '/message/send', message);
  }

}
