import { User } from '../login/user';
import { Room } from './room';

export class Message {
  content: string;
  user: User;
  dateTime: Date;
  room: Room;

  constructor() {
    this.content = '';
    this.user = new User();
    this.dateTime = new Date();
    this.room = new Room();
  }
}
