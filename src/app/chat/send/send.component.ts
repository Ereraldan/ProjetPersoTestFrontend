import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../messages.service';

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.css']
})
export class SendComponent implements OnInit {
  message: string;
  roomId: string;

  constructor(private messageService: MessagesService) {
    this.message = '';
  }

  ngOnInit() {
  }

  send() {
    this.messageService.send(this.message, this.roomId);
    this.message = '';
  }
}
