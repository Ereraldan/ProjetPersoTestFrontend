import { User } from '../login/user';

export class Room {
  id: string;
  contributor: User[];

  constructor() {
    this.contributor = [];
  }
}
