import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  @ViewChild('messagesBox') messagesBox: ElementRef;

  constructor() {
  }

  ngOnInit() {
    this.messagesBox.nativeElement.scrollTop = this.messagesBox.nativeElement.scrollHeight;
  }
}
