import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routes';
import { AuthGuard } from './auth.guard';
import { ChatComponent } from './chat/chat.component';
import { MessagesService } from './chat/messages.service';
import { MessagesComponent } from './chat/messages/messages.component';
import { SendComponent } from './chat/send/send.component';
import { UserListComponent } from './chat/user-list/user-list.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChatComponent,
    UserListComponent,
    MessagesComponent,
    SendComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutes,
    HttpClientModule
  ],
  providers: [LoginService, AuthGuard, MessagesService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
