import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './user';
import { LoginService } from './login.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;

  constructor(private loginService: LoginService, private router: Router) {
    this.user = new User();
  }

  ngOnInit() {
    localStorage.clear();
  }

  log() {
    if ((this.user.login && this.user.password) !== '') {
      this.loginService.login(this.user).toPromise().then((data: any) => {
        localStorage.setItem('token', data.token);
        this.router.navigateByUrl('/chat');
      }).catch((error) => {
        console.log(error.error.message);
      });
    } else {
      console.log('can\'t connect missing username or password');
    }
  }

}
