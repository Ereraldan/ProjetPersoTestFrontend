import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { User } from './user';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) {
  }

  login(userLogin: User) {
    return this.http.post(environment.url + 'login', userLogin);
  }
}
